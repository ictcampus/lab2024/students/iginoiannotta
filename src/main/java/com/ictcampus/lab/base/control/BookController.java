package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.model.BookResponse;
import com.ictcampus.lab.base.control.model.BookRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Controller per la gestione delle operazioni sui libri.
 * Fornisce API RESTful per creare, leggere, aggiornare e cancellare i libri.
 */
@RestController
@RequestMapping("/api/v1/books")
@AllArgsConstructor
@Slf4j
public class BookController {

	private List<BookResponse> list = new ArrayList<>();

	/**
	 * Costruttore del BookController.
	 * Inizializza la lista dei libri con alcuni valori predefiniti.
	 */
	public BookController() {
		this.list = generateBooks();
	}

	/**
	 * Recupera una lista di tutti i libri.
	 *
	 * @return Una lista di BookResponse.
	 */
	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<BookResponse> getBooks() {
		log.info("Restituzione della lista di libri: {}", list);
		return this.list;
	}

	/**
	 * Recupera i dettagli di un libro specifico dato il suo ID.
	 *
	 * @param id L'ID del libro da recuperare.
	 * @return Il BookResponse che rappresenta il libro.
	 * @throws ChangeSetPersister.NotFoundException Se il libro con l'ID specificato non esiste.
	 */
	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public BookResponse getBook(@PathVariable(name = "id") Long id) throws ChangeSetPersister.NotFoundException {
		BookResponse bookResponse = this.findBookById(id);
		if (bookResponse == null) {
			log.warn("Libro con ID [{}] non trovato", id);
			throw new ChangeSetPersister.NotFoundException();
		}

		log.info("Restituzione del libro con ID [{}]: {}", id, bookResponse);
		return bookResponse;
	}

	/**
	 * Cerca libri per titolo.
	 *
	 * @param title Il titolo del libro da cercare.
	 * @return Una lista di BookResponse che corrispondono al titolo cercato.
	 */
	@GetMapping(value = "/title/{title}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<BookResponse> searchByTitle(@PathVariable(name = "title") String title) {
		log.info("Ricerca di libri con titolo contenente: {}", title);
		List<BookResponse> results = new ArrayList<>();
		for (BookResponse book : list) {
			if (book.getTitle().toLowerCase().contains(title.toLowerCase())) {
				results.add(book);
			}
		}
		log.info("Libri trovati: {}", results);
		return results;
	}

	/**
	 * Crea un nuovo libro basato sui dati forniti.
	 *
	 * @param bookRequest I dati del nuovo libro da creare.
	 * @return L'ID del libro creato.
	 */
	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public Long createBook(@RequestBody BookRequest bookRequest) {
		log.info("Creazione di un nuovo libro con i dati [{}]", bookRequest);
		Long id = this.lastId();
		log.info("ID del nuovo libro: [{}]", id);

		String title = bookRequest.getTitle() != null ? bookRequest.getTitle() : "Titolo Sconosciuto";
		String author = bookRequest.getAuthor() != null ? bookRequest.getAuthor() : "Autore Sconosciuto";
		String isbn = bookRequest.getIsbn() != null ? bookRequest.getIsbn() : "ISBN Sconosciuto";
		String description = bookRequest.getDescription() != null ? bookRequest.getDescription() : "Nessuna Descrizione";

		BookResponse bookResponse = new BookResponse(id, title, author, isbn, description);
		this.list.add(bookResponse);

		log.info("Libro creato: {}", bookResponse);
		return id;
	}

	/**
	 * Aggiorna i dettagli di un libro esistente.
	 *
	 * @param id L'ID del libro da aggiornare.
	 * @param bookRequest I nuovi dati del libro.
	 */
	@PutMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void editBook(@PathVariable(name = "id") Long id, @RequestBody BookRequest bookRequest) {
		log.info("Aggiornamento del libro con ID [{}] e nuovi dati [{}]", id, bookRequest);
		this.updateById(id, bookRequest);
		log.info("Libro con ID [{}] aggiornato con successo", id);
	}

	/**
	 * Elimina un libro dato il suo ID.
	 *
	 * @param id L'ID del libro da eliminare.
	 */
	@DeleteMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void deleteBook(@PathVariable(name = "id") Long id) {
		log.info("Eliminazione del libro con ID [{}]", id);
		this.deleteById(id);
		log.info("Libro con ID [{}] eliminato con successo", id);
	}

	/**
	 * Mescola la lista dei libri.
	 *
	 * @return La lista dei libri mescolata.
	 */
	@GetMapping(value = "/shuffle", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<BookResponse> shuffleBooks() {
		log.info("Mescolamento della lista di libri.");
		Collections.shuffle(this.list);
		log.info("Lista di libri mescolata: {}", list);
		return this.list;
	}

	/**
	 * Genera una lista predefinita di libri.
	 *
	 * @return Una lista di BookResponse.
	 */
	private List<BookResponse> generateBooks() {
		Long index = 1L;
		return new ArrayList<>(Arrays.asList(
				new BookResponse(index++, "La Divina Commedia", "Dante Alighieri", "9788807900317", "Un viaggio epico attraverso l'Inferno, il Purgatorio e il Paradiso."),
				new BookResponse(index++, "Don Chisciotte della Mancia", "Miguel de Cervantes", "9788854168992", "Le avventure del cavaliere errante Don Chisciotte."),
				new BookResponse(index++, "Guerra e Pace", "Lev Tolstoj", "9788807900089", "Un'epopea storica sulle vite di diverse famiglie durante le guerre napoleoniche."),
				new BookResponse(index++, "Il Conte di Montecristo", "Alexandre Dumas", "9788807901253", "La storia di un uomo ingiustamente imprigionato che cerca vendetta."),
				new BookResponse(index++, "Cime Tempestose", "Emily Brontë", "9788807900027", "Una tragica storia d'amore e vendetta ambientata nelle brughiere inglesi.")
		));
	}

	/**
	 * Trova un libro per ID.
	 *
	 * @param id L'ID del libro da trovare.
	 * @return Il BookResponse se trovato, altrimenti null.
	 */
	private BookResponse findBookById(Long id) {
		return this.list.stream()
				.filter(book -> book.getId().equals(id))
				.findFirst()
				.orElse(null);
	}

	/**
	 * Ottiene l'ultimo ID disponibile per un nuovo libro.
	 *
	 * @return Il prossimo ID disponibile.
	 */
	private Long lastId() {
		return this.list.stream()
				.mapToLong(BookResponse::getId)
				.max()
				.orElse(0L) + 1;
	}

	/**
	 * Aggiorna i dettagli di un libro esistente.
	 *
	 * @param id L'ID del libro da aggiornare.
	 * @param newData I nuovi dati del libro.
	 */
	private void updateById(Long id, BookRequest newData) {
		this.list.stream()
				.filter(book -> book.getId().equals(id))
				.findFirst()
				.ifPresent(book -> {
					book.setTitle(newData.getTitle() != null ? newData.getTitle() : book.getTitle());
					book.setAuthor(newData.getAuthor() != null ? newData.getAuthor() : book.getAuthor());
					book.setIsbn(newData.getIsbn() != null ? newData.getIsbn() : book.getIsbn());
					book.setDescription(newData.getDescription() != null ? newData.getDescription() : book.getDescription());
				});
	}

	/**
	 * Elimina un libro dato il suo ID.
	 *
	 * @param id L'ID del libro da eliminare.
	 */
	private void deleteById(Long id) {
		this.list.removeIf(book -> book.getId().equals(id));
	}
}
