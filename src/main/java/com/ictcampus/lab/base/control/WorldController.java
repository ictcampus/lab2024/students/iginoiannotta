package com.ictcampus.lab.base.control.world;

import com.ictcampus.lab.base.control.exception.NotFoundException;
import com.ictcampus.lab.base.control.world.model.WorldRequest;
import com.ictcampus.lab.base.control.world.model.WorldResponse;
import com.ictcampus.lab.base.service.world.WorldService;
import com.ictcampus.lab.base.service.world.model.World;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller per la gestione delle operazioni sui mondi.
 * Fornisce API RESTful per creare, leggere, aggiornare e cancellare i mondi.
 * <p>
 * Autore: Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@RestController
@RequestMapping("/api/v1/worlds")
@AllArgsConstructor
@Slf4j
public class WorldController {

	@Autowired
	private final WorldService worldService;

	/**
	 * Recupera una lista di mondi filtrati per nome e/o termine di ricerca.
	 *
	 * @param name Il nome del mondo da cercare (opzionale).
	 * @param search Un termine di ricerca generico (opzionale).
	 * @return Una lista di WorldResponse che corrispondono ai criteri di ricerca.
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<WorldResponse> getWorlds(@RequestParam(required = false) String name,
										 @RequestParam(required = false) String search) {
		log.info("Richiesta di ottenere mondi con nome [{}] e termine di ricerca [{}]", name, search);
		return worldService.getWorlds(name, search).stream()
				.map(this::convertToWorldResponse)
				.collect(Collectors.toList());
	}

	/**
	 * Recupera i dettagli di un mondo specifico dato il suo ID.
	 *
	 * @param id L'ID del mondo da recuperare.
	 * @return Il WorldResponse che rappresenta il mondo.
	 * @throws NotFoundException Se il mondo con l'ID specificato non esiste.
	 */
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public WorldResponse getWorld(@PathVariable("id") Long id) throws NotFoundException {
		log.info("Richiesta di ottenere il mondo con ID [{}]", id);
		World world = worldService.getWorldById(id);
		if (world == null) {
			log.warn("Mondo con ID [{}] non trovato", id);
			throw new NotFoundException("Mondo non trovato");
		}

		WorldResponse response = convertToWorldResponse(world);
		log.info("Restituzione del mondo con ID [{}] e dati [{}]", id, response);
		return response;
	}

	/**
	 * Crea un nuovo mondo basato sui dati forniti.
	 *
	 * @param worldRequest I dati del nuovo mondo da creare.
	 * @return L'ID del mondo creato.
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Long createWorld(@RequestBody WorldRequest worldRequest) {
		log.info("Richiesta di creazione di un nuovo mondo con i dati [{}]", worldRequest);
		Long newWorldId = worldService.addWorld(worldRequest.getName(), worldRequest.getSystem());
		log.info("Mondo creato con ID [{}]", newWorldId);
		return newWorldId;
	}

	/**
	 * Aggiorna i dettagli di un mondo esistente.
	 *
	 * @param id L'ID del mondo da aggiornare.
	 * @param worldRequest I nuovi dati del mondo.
	 */
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void editWorld(@PathVariable("id") Long id, @RequestBody WorldRequest worldRequest) {
		log.info("Richiesta di aggiornamento del mondo con ID [{}] con i nuovi dati [{}]", id, worldRequest);
		worldService.editWorld(id, worldRequest.getName(), worldRequest.getSystem());
		log.info("Mondo con ID [{}] aggiornato con successo", id);
	}

	/**
	 * Elimina un mondo dato il suo ID.
	 *
	 * @param id L'ID del mondo da eliminare.
	 */
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteWorld(@PathVariable("id") Long id) {
		log.info("Richiesta di eliminazione del mondo con ID [{}]", id);
		worldService.deleteWorld(id);
		log.info("Mondo con ID [{}] eliminato con successo", id);
	}

	/**
	 * Converte un oggetto World in WorldResponse.
	 *
	 * @param world L'oggetto World da convertire.
	 * @return L'oggetto WorldResponse risultante.
	 */
	private WorldResponse convertToWorldResponse(World world) {
		WorldResponse response = new WorldResponse();
		response.setId(world.getId());
		response.setName(world.getName());
		response.setSystem(world.getSystem());
		return response;
	}
}